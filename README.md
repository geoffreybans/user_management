This is a user management application. It enables you to sign up for an account, login, logout and update your profile. Once logged in it shows you a dummy sales dashboard at the homepage.

### What can this application do? ###

* Enables you sign up for an account
* Has email verification before you can access your account
* Enables you to login into your account
* Times failed login attempts and suspends your account after 3 trials, the default.
* You can change the maximum number of login attempts as an admin user.
* All new accounts are regular user accounts by default
* After suspension, you cannot login even if your remembered your password right, this time round.
* Suspension of account is automatically lifted after 10 minutes wait and you can attempt to login again.
* You can change the duration of the account suspension time as an admin user
* Version 0.0.1
* [Click this link to view the live app on heroku](https://user-management-7733.herokuapp.com)

### How do I get set up? ###

These steps assume that you already have set up your Ruby on Rails development environment.

* Download the source code to your `localhost`
* Change directory to the root of your application in the terminal
* Run the `bundle install` to install the required gems
* Run `rails server` to start the WEBrick server
* Access the app through, for example `http://localhost:3000`


### How do I use it? ###

* If you don't have an account yet, click on the sign up link at the bottom of the login form to create an account
* After successful sign up, check for a link in your email to verify your email address and, therefore,activate your account.
* Visit the homepage to login
* Though after email verification you are automatically logged in for the first time.
* Click on the gravatar icon to log out.
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin