class CreateSettings < ActiveRecord::Migration
  def change
    create_table :settings do |t|
      t.integer :login_attempts
      t.integer :account_suspend_time
      t.integer :expire_reset_link

      t.timestamps null: false
    end
  end
end
