class AppController < ThrottleController

	skip_before_action :require_login

	def index
	end

	def contact		
	end

	def clients		
	end

	def blog		
	end

	def home	
		render "index"	
	end
end
