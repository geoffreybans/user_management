// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require jquery.turbolinks
//= require_tree .


//form helper module
var Lib = Lib || {};

Lib.FormHelper = (function(){
	
	/**
	 *This method diables the form submit button upon click
	 *by setting the disbled="disabled" attribute
	 *@param Object e The event trigger object
	 *@return null This method does not return anything
	 */
	function stopDoubleSubmit(e){
		//diable submit button
		var node = e.target;
		node.setAttribute("disabled", "disbabled");
	}

	/**
	 *This method displays the value of an input field throught the alert() function
	 *@param Object e The event trigger object
	 *@return null This method does not return anything
	 */
	function displayContent(e){
		alert(e.target.value);
	}

	/**
	 *This method checks if the email address hasnt been used before
	 *@param Object e The event strigger object
	 *@return Updates the associated text field in the browser with the message
	 */
	function checkEmail(e){
		//define arguments to send to the server
		var url = checkEmailLink + "/" + e.target.value;

		Lib.Ajax.get({

			url: url,
			dataType: 'json',
			success: setEmailMessage,
			error: function(data){
				console.log(data)
			}

		});

	}

	//update the email verifty fields as appropriate
	function setEmailMessage(data){
		
		//email already exists
		if (data.found && data.found == true) {
			var node = document.getElementById('unique-email-verify');
			node.innerHTML = "<b>This is email address is already in use!</b>";
			node.className = "text-danger";

			var node = document.getElementById('email-verified');
			node.className = 'btn btn-danger';
			node.innerHTML = '<i class="glyphicon glyphicon-remove-circle"></i>';

			node.parentNode.parentNode.className += ' has-error';
		}
		//emails doesn't exists
		else{
			var node = document.getElementById('unique-email-verify');
			node.innerHTML = "";
			node.className = "";

			var node = document.getElementById('email-verified');
			node.className = 'btn btn-success';
			node.innerHTML = '<i class="glyphicon glyphicon-ok-circle"></i>';

			node.parentNode.parentNode.className = Lib.DOMHelper.removeClass(node.parentNode.parentNode.className, 'has-error');

		}
	}

	/**
	 *This check and displays the password strength using a simple algorithm
	 *@param {} e The event trigger object
	 *@return null This method does not return any value
	 */
	function passwordStrength(e){

		var password = e.target.value;

		//count uppercase letters
		var uppercase = password.match(/[A-Z]/g);
		uppercase = (uppercase && uppercase.length || 0);

		//count numbers
		var numbers = password.match(/\d/g);
		numbers = (numbers && numbers.length || 0);

		//count the symbols
		var symbols = password.match(/\w/g);
		symbols = (symbols && symbols.length || 0);

		//determing the strength
		var strength = password.length + uppercase + (numbers * 2) + (symbols * 3);
		strength = Math.min(Math.floor(strength / 10), 2);

		var node = {};

		if (strength < 1) {
			node = document.getElementById('password-strength');
			node.className = 'btn btn-danger';
			node.innerHTML = '<i class="glyphicon glyphicon-remove-circle"></i>';
		}
		else if(strength < 2){
			node = document.getElementById('password-strength');
			node.className = 'btn btn-warning';
			node.innerHTML = '<i class="glyphicon glyphicon-ok-circle"></i>';
		} 
		else {
			node = document.getElementById('password-strength');
			node.className = 'btn btn-success';
			node.innerHTML = '<i class="glyphicon glyphicon-ok-circle"></i>';
		}

	}

	/**
	 *This method checks if the two password entry fields have the same value
	 *And updates the message area as appropriate
	 *@param {} e The event trigger object
	 *@return null This method does not return any value
	 */
	 function comparePasswords(e){
	 	//passwords do not match
	 	if (e.target.value !== document.getElementById('password').value) {

			var node = document.getElementById('password-error');
			node.innerHTML = "<b>The two passwords do not match!<b>";
			node.className = "text-danger";

			var node = document.getElementById('password-verified');
			node.className = 'btn btn-danger';
			node.innerHTML = '<i class="glyphicon glyphicon-remove-circle"></i>';

			e.target.parentNode.className += ' has-error';

	 	} 
	 	//passowords match
	 	else {

			var node = document.getElementById('password-error');
			node.innerHTML = "";
			node.className = "";

			var node = document.getElementById('password-verified');
			node.className = 'btn btn-success';
			node.innerHTML = '<i class="glyphicon glyphicon-ok-circle"></i>';

			e.target.parentNode.className = Lib.DOMHelper.removeClass(e.target.parentNode.className, 'has-error');
	 	}
	 }

	/**
	 *This are the methods available for use outside of this module
	 *access using the alias to the right hand side
	 */
	return {
		stopDoubleSubmit: stopDoubleSubmit,
		displayContent : displayContent,
		checkEmail: checkEmail,
		comparePasswords: comparePasswords,
		passwordStrength: passwordStrength
	}

}());

/*
 *This module has general helper methods that simply certain tasks
 *includes = disable spacebar, remove class e.t.c
 *
 */
Lib.DOMHelper = (function(){

	/**
	 *This method disables the spacebar key
	 *@param Object e The eventtriggered object
	 *@return void This method does not return anything
	 */
	function noSpaces(e){
		if (e.charCode == 32) {
			e.preventDefault();
		}
	}

	/**
	 *This method removes classes from an element
	 *@param string current The string containing the classes
	 *@param string remove The particular class to remove
	 *@return string The final class string after remove
	 */
	function removeClass(current, remove){
		var newclass = "";
		var classList = current.split(" ");

		//loop to remove this particular class
		for(var i = 0; i < classList.length; i++){
			if(classList[i].trim() !== remove){
				newclass += classList[i].trim() + " ";
			}
		}

		return newclass;
	}

	/**
	 *These are the methods exposed to outside use
	 *Acess using the name alias to the right
	 */
	return {
		removeClass: removeClass,
		noSpaces: noSpaces
	}

}());

/*
 *This module helps in making ajax calls to the server
 *It takes one parameter, an object containing the parameters of the call
 *available methods are post, get, ajax
 *post = only post requests
 *get = only get requests
 *ajax = both post/get. specify with method attribute in the call
*/
Lib.Ajax = (function(){

	/**
	 *This method makes an ajax request
	 *@param Object e The object with request parameters
	 *@return void The method executes the callback methods provided in the params
	 */
	function ajax(e){
		
		//create an instance of the xhttp req
		var xhr = new XMLHttpRequest();
		var method = e.method;
		var data = e.data || {};

		xhr.open(method.toUpperCase(), e.url, e.async || true);
		
		//do something on success
		xhr.onreadystatechange = function(){
			if (xhr.readyState == 4 ) {
				if(xhr.status == 200) {
					if (e.dataType && e.dataType.toUpperCase() == 'JSON') {
							e.success(JSON.parse(xhr.responseText));
					}
					else{
						e.success(xhr.responseText);
					}	
				}
				else{
					e.error(xhr.responseText);
				}
			}
		};

		//make the request
		xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded; charset=UTF-8");
		xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
		xhr.send(data);

	}

	/**
	 *This method makes a get request
	 *@param Object e The object with request parameters
	 *@return void The method executes the callback methods provided in the params
	 */
	function get(e){
		
		//create an instance of the xhttp req
		var xhr = new XMLHttpRequest();
		xhr.open("GET", e.url, e.async || true);
		
		//do something on success
		xhr.onreadystatechange = function(){
			if (xhr.readyState == 4 ) {
				if(xhr.status == 200) {
					if (e.dataType && e.dataType.toUpperCase() == 'JSON') {
							e.success(JSON.parse(xhr.responseText));
					}
					else{
						e.success(xhr.responseText);
					}
				}
				else{
					e.error(xhr.responseText);
				}
			}
		};

		//make the request
		xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded; charset=UTF-8");
		xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
		xhr.send();

	}

	/**
	 *This method makes a post request
	 *@param Object e The object with request parameters
	 *@return void The method executes the callback methods provided in the params
	 */
	function post(e){
		
		//create an instance of the xhttp req
		var xhr = new XMLHttpRequest();
		xhr.open("POST", e.url, e.async || true);
		
		//do something on response
		xhr.onreadystatechange = function(){
			if (xhr.readyState == 4 ) {
				if(xhr.status == 200) {
					if (e.dataType && e.dataType.toUpperCase() == 'JSON') {
							e.success(JSON.parse(xhr.responseText));
					}
					else{
						e.success(xhr.responseText);
					}
				}
				else{
					e.error(xhr.responseText);
				}
			}
		};

		//make the request
		xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded; charset=UTF-8");
		xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
		xhr.send(e.data);

	}

	/**
	 *These are the exposed public method available to the outside
	 *access using the name alias to the right
	 */
	return {
		ajax: ajax,
		post: post,
		get: get
	}

}());

