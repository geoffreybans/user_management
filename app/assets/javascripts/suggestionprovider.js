//set the module namespace
var Lib = Lib || {};

//define the suggestions provider
Lib.SuggestionProvider = (function(){

	var states = [
	        "Alabama", "Alaska", "Arizona", "Arkansas",
	        "California", "Colorado", "Connecticut",
	        "Delaware", "Florida", "Georgia", "Hawaii",
	        "Idaho", "Illinois", "Indiana", "Iowa",
	        "Kansas", "Kentucky", "Louisiana",
	        "Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota",
	        "Mississippi", "Missouri", "Montana",
	        "Nebraska", "Nevada", "New Hampshire", "New Mexico", "New York",
	        "North Carolina", "North Dakota", "Ohio", "Oklahoma", "Oregon",
	        "Pennsylvania", "Rhode Island", "South Carolina", "South Dakota",
	        "Tennessee", "Texas", "Utah", "Vermont", "Virginia",
	        "Washington", "West Virginia", "Wisconsin", "Wyoming"
	    ];

	function requestSuggestions(input, target){
		var suggestions = [];
		//find suggestions
		if(input.length > 0){
			for(var i = 0; i < states.length; i++){
				if (states[i].toUpperCase().indexOf(input.toUpperCase()) == 0) {
					suggestions.push(states[i]);
				}
			}
		}

		return suggestions;
	}

	return {
		requestSuggestions: requestSuggestions
	}

}());