class CreateRequests < ActiveRecord::Migration
  def change
    create_table :requests do |t|
      t.integer :user_id
      t.string :ip_address
      t.string :country
      t.string :state
      t.string :city
      t.string :zip
      t.string :provider
      t.string :uuid
      t.string :request_header
      t.string :request_url
      t.string :request_path
      t.string :request_method
      t.string :media_type
      t.boolean :local
      t.boolean :xhttp
      t.string :flash_content
      t.string :user_agent
      t.string :server_software
      t.string :device
      t.string :os
      t.string :browser
      t.string :more
      t.string :cookie

      t.timestamps null: false
    end
  end
end
