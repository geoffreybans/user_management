class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :password
      t.string :role
      t.boolean :activated
      t.string :activation_code
      t.string :ip_address
      t.string :dob
      t.string :p_address
      t.string :p_code
      t.string :city
      t.string :h_phone
      t.string :w_phone

      t.timestamps null: false
    end
  end
end
