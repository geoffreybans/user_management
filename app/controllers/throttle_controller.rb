class ThrottleController < ApplicationController

	before_action :require_login
	before_action :set_request_details

	private

	def require_login
		if session[:user_id].nil?
			flash[:error] = "Login required!"
			redirect_to "/users/login"
		end
	end

	def set_request_details
		@req = Request.new

		#fill in the details of the request
		@req.user_id = session[:user_id] || nil
		@req.ip_address = request.remote_ip()
		@req.country = nil
		@req.state = nil
		@req.city = nil
		@req.zip = nil
		@req.provider = nil
		@req.uuid = request.uuid()
		@req.request_header = request.headers()
		@req.request_url = request.original_url()
		@req.request_path = request.original_fullpath()
		@req.request_method = request.request_method()
		@req.media_type = request.media_type()
		@req.local = request.local?
		@req.xhttp = request.xhr?
		@req.flash_content = request.flash()
		@req.user_agent = request.user_agent()
		@req.server_software = request.server_software()
		@req.device = nil
		@req.os = nil
		@req.browser = nil
		@req.more = nil
		@req.cookie = request.cookie_jar()

		@req.save
	end
end
