class CreateGravatars < ActiveRecord::Migration
  def change
    create_table :gravatars do |t|
      t.integer :user_id
      t.string :image

      t.timestamps null: false
    end
  end
end
