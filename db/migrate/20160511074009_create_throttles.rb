class CreateThrottles < ActiveRecord::Migration
  def change
    create_table :throttles do |t|
      t.integer :user_id
      t.integer :attempts
      t.string :last
      t.boolean :suspended
      t.string :time
      t.string :ip
      t.string :token
      t.integer :lifetime

      t.timestamps null: false
    end
  end
end
