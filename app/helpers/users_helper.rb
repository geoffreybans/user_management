module UsersHelper

	def get_title(sub_title = "")
		if sub_title.empty?
			"User Management App"
		else
			sub_title + " | " + "User Management App"
		end
	end
end
