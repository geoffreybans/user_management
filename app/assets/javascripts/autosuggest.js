//set the module namespace
var Lib = Lib || {};

/**
 *This module provides autosuggestion while typing into a text box
 *It relies on a set of suggestions provided by the SuggestionsProvider module
 *It attemps to read ahead as you type and attempt to prefill the text entry field
 */
Lib.AutoSuggest = (function(){

	/**
	 *@param {} The event tigger targeted textbox
	 */
	var textbox;

	/**
	 *@param {} The suggestions provider
	 */
	var provider;

	/**
	 *@param {} The reference to the dropdown DOM div list
	 */
	var layer;

	/**
	 *@param Integer The index of the current suggestion in the suggestions array
	 */
	var current = -1;

	/**
	 *This is the entry point into the autosuggest module
	 *@param {} e The keyup event trigger object
	 *@return null This method does not return any value
	 */
	function handle(event){
		//check if screen is resized and adjust the dropdown width
		layer.style.width = textbox.offsetWidth + "px";

		switch(event.type){
			case "keyup":
				var keyed = handleKeyup(event);

				//only act for valid keypress
				if(keyed.character === true){
					//get suggestion from the provider
					autosuggest(Lib.SuggestionProvider.requestSuggestions(textbox.value, textbox), keyed.typeAhead); 
				}
				break;
			case "keydown":
				handleKeydown(event);
				break;
			case "blur":
				hideSuggestions();
				break;
		}

	}

	/**
	 *This method checks if the keyup event involved a character key,
	 *All non character keys are to be ignored
	 *@param {} e The keyup event trigger object
	 *@return {} true|false True if character key and typeAhead, otherwise false
	 */
	function handleKeyup(event){
		var keycode = event.keyCode;

		if(keycode == 8 || keycode == 46){
			//do not type ahead for backspace and delete
			return {
				character: true,
				typeAhead: false
			}
		}
		if(keycode < 32 || (keycode >= 33 && keycode <= 46) || (keycode >= 112 && keycode <= 123)){
			//this is not a character key
			return {
				character: false,
				typeAhead: false
			}
		}
		else{
			//this is a character key
			return {
				character: true,
				typeAhead: true
			}
		}
	}

	/**
	 *This method handles the up arrow, down arrow and enter keys and requires the event object to be passed.
	 *
	 *@param {} event The keydown event trigger object
	 *@param null This method does not return any value
	 */
	function handleKeydown(event){
		switch(event.keyCode){
			case 38: //up arrow
				previousSuggestion();
				break;
			case 40: //down arrow
				nextSuggestion();
				break;
			case 13: //enter
				hideSuggestions();
				break;

		}
	}
	/**
	 *This method provides the first suggestion for autocomplete type ahead
	 *@param [] suggestions Array of the available suggestions
	 *@param Bool true|false typeAhead Indicates whether or not the type ahead functionality should be used
	 */
	function autosuggest(suggestions, type){
		console.log(suggestions);
		if(suggestions.length > 0){
			if (type) {
				typeAhead(suggestions[0]);
			}
			showSuggestions(suggestions);			
		}
		else{
			hideSuggestions();
		}
	}

	/**
	 *This method completes the word while the user is still typing
	 *@param String suggestion The work/phrase suggestion to prefill
	 *@return null This method does not return any value
	 */
	function typeAhead(suggestion){
		if(textbox.createTextRange || textbox.setSelectionRange){
			var len = textbox.value.length;
			textbox.value = suggestion;
			selectRange(len,suggestion.length);
		}
	}

	/**
	 *This method defines the uncompleted part of the suggestion to high in a separate color
	 *The part already typed by user is not to be highlighted.
	 *Highlighting is only done for EI and FireFox browsers
	 *@param Integer start The character index from where to start highlighting
	 *@param Integer length The full length of the suggested word/phrase
	 *@return null This method does not return a value 
	 */
	function selectRange(start, length){
		if(textbox.createTextRange){
			var range = textbox.createTextRange();
			range.moveStart('character', start);
			range.moveEnd('character', length - textbox.value.length);
			range.select();
		}
		else if(textbox.setSelectionRange){
			textbox.setSelectionRange(start, length);
		}

		textbox.focus();
	}

	/**
	 *This method hides the autosuggestion dropdown list after it has been show
	 *
	 *@param null This method does not take any parameter
	 *@return null This method does not return any value
	 */
	function hideSuggestions(){
		layer.style.visibility = "hidden";
	}

	/**
	 *This method highlights the current suggestion in the dropdown list
	 *
	 *@param {} suggestionNode The div element of the current suggestion
	 *@return null This method does not return any value
	 */
	function highlightSuggestion(suggestionNode){
		for(var i = 0; i < layer.childNodes.length; i++){
			var node = layer.childNodes[i];

			if (node == suggestionNode) {
				node.className = "current";
			}
			else if(node.className == "current"){
				node.className = "";
			}
		}
	}

	/**
	 *This method creates the outermost <div> and defines the event
	 *handlers for the dropdown list
	 *
	 *@param {} inputbox The textbox element object reference
	 *@return null This method does not return any value
	 */
	function createDropdown(inputbox = null){
		//set the textbox property value
		textbox = textbox || inputbox;

		layer = document.createElement("div");
		layer.className = "suggestions";
		layer.style.visibility = "hidden";
		layer.style.width = textbox.offsetWidth + "px";

		document.body.appendChild(layer);

		//assign the event handlers
		layer.onmousedown = layer.onmouseup = layer.onmouseover = function(event){
			var event = event || window.event;
			var target = event.target || event.srcElement;

			if (event.type == "mousedown") {
				textbox.value = target.firstChild.nodeValue;
				hideSuggestions();
			} 
			else if(event.type == "mouseover"){
				highlightSuggestion(target);
			}
			else{
				textbox.focus();
			}
		}
	}

	/**
	 *This method tells us how many pixels away from the left of the offsetParent the textbox is.
	 *We calculate this all the way upto the <body> tag
	 *
	 *@param null This method does not take any parameter
	 *@return Integer The number of pixels from the left
	 */
	function getLeft(){
		var node = textbox;
		var left = 0;

		while(node.tagName != "BODY"){
			left += node.offsetLeft;
			node = node.offsetParent;
		}

		return left;
	}

	/**
	 *This method tells us how many pixels away from the top of the offsetParent the textbox is.
	 *We calculate this all the way upto the <body> tag
	 *
	 *@param null This method does not take any parameter
	 *@return Integer The number of pixels from the left
	 */
	function getTop(){
		var node = textbox;
		var top = 0;

		while(node.tagName != "BODY"){
			top += node.offsetTop;
			node = node.offsetParent;
		}

		return top;		
	}

	/**
	 *This method accepts an array of suggestions as an argument and then adds the 
	 *suggestions into the dropdown list and displays it
	 *
	 *@param [] suggestion The array with the suggestions to show
	 *@return null This method does not return a value
	 */
	function showSuggestions(suggestions){
		var div = null;
		layer.innerHTML = "";

		//loop through the suggestions array adding one at a time
		for( var i = 0; i < suggestions.length; i++){
			div = document.createElement("div");
			div.appendChild(document.createTextNode(suggestions[i]));
			layer.appendChild(div);
		}

		//position and show the dropdown list
		layer.style.left = getLeft() + "px";
		layer.style.top = (getTop() + textbox.offsetHeight) + "px";
		layer.style.visibility = "visible";
	}

	/**
	 *This method ensures that when the down arrow key is pressed, the next suggesion in the dropdown list is higlighted
	 *
	 *@param null This method does not require any parameter
	 */
	function nextSuggestion(){
		var suggestionNodes = layer.childNodes;

		if (suggestionNodes.length > 0 && current < suggestionNodes.length - 1) {
			var node = suggestionNodes[++current];
			highlightSuggestion(node);

			textbox.value = node.firstChild.nodeValue;
		}
	}	

	/**
	 *This method ensures that when the up arrow key is pressed, the prevous suggesion in the dropdown list is higlighted
	 *
	 *@param null This method does not require any parameter
	 */
	function previousSuggestion(){
		var suggestionNodes = layer.childNodes;

		if (suggestionNodes.length > 0 && current > 0) {
			var node = suggestionNodes[--current];
			highlightSuggestion(node);

			textbox.value = node.firstChild.nodeValue;
		}
	}

	return {
		handle: handle,
		createDropdown: createDropdown
	}

}());