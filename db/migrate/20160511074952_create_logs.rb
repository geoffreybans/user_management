class CreateLogs < ActiveRecord::Migration
  def change
    create_table :logs do |t|
      t.string :user_id
      t.string :time_in
      t.string :time_out
      t.string :ip_address
      t.string :country
      t.string :city
      t.string :state
      t.string :zip

      t.timestamps null: false
    end
  end
end
