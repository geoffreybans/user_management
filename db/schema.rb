# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160621075024) do

  create_table "gravatars", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "image"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "logs", force: :cascade do |t|
    t.string   "user_id"
    t.string   "time_in"
    t.string   "time_out"
    t.string   "ip_address"
    t.string   "country"
    t.string   "city"
    t.string   "state"
    t.string   "zip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "requests", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "ip_address"
    t.string   "country"
    t.string   "state"
    t.string   "city"
    t.string   "zip"
    t.string   "provider"
    t.string   "uuid"
    t.string   "request_header"
    t.string   "request_url"
    t.string   "request_path"
    t.string   "request_method"
    t.string   "media_type"
    t.boolean  "local"
    t.boolean  "xhttp"
    t.string   "flash_content"
    t.string   "user_agent"
    t.string   "server_software"
    t.string   "device"
    t.string   "os"
    t.string   "browser"
    t.string   "more"
    t.string   "cookie"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "settings", force: :cascade do |t|
    t.integer  "login_attempts"
    t.integer  "account_suspend_time"
    t.integer  "expire_reset_link"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "throttles", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "attempts"
    t.string   "last"
    t.boolean  "suspended"
    t.string   "time"
    t.string   "ip"
    t.string   "token"
    t.integer  "lifetime"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "password"
    t.string   "role"
    t.boolean  "activated"
    t.string   "activation_code"
    t.string   "ip_address"
    t.string   "dob"
    t.string   "p_address"
    t.string   "p_code"
    t.string   "city"
    t.string   "h_phone"
    t.string   "w_phone"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

end
