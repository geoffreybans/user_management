class UsersController < ThrottleController
	
	skip_before_action :require_login, only: [:new, :create, :confirm, :login, :checkemail]
	
	#load the sign up form	
	def new	
		@user = User.new
	end

	def create
		@user = User.new
		input = params[:user]
		@user.first_name = input[:first_name]
		@user.last_name = input[:last_name]
		@user.email = input[:email]

		digest = Digest::MD5.new
		digest << input[:password]

		@user.password = digest.hexdigest
		@user.activated = false
		@user.role = 'user'
		
		#generate activation code
		rand = Random.new_seed
		rand = Base64.encode64(rand.to_s)
		rand.chomp! #remove carriage return

		@user.activation_code = rand

		#sign up success, email password confirmation
		if @user.save
			UserMailer.welcome_email(@user).deliver
			render 'signup_success'
		#reload the signup page with error messages
		else
			render 'new'
		end
	end
	
	#check for valid user id and activation code
	def confirm
		if !params[:id].nil? && !params[:activation_code].nil?
			@user = User.find_by(id: params[:id])

			if !@user.nil?
				if @user.activation_code == params[:activation_code]
					#confirmation success, authenticate
					@user.activated = true
					@user.activation_code = ""
					@user.save

					session[:user_id] = @user.id
					session[:gravatar] = @user.gravatar.image
					session[:role] = @user.role
					
					redirect_to users_url
				else
					flash[:danger] = "Invalid Activation Link!"
					render 'login'
				end
			else
				flash[:danger] = "User Cannot be found!"
				render 'login'
			end
		else
			flash[:danger] = "Invalid Activation Link!"
			render 'login'
		end
	end
	
	#display the user home page with dummy dashboard
	def index
		@user = User.find(session[:user_id])
		@gravatar = @user.gravatar
	end

	#show the user's profile information
	def show
		@user = User.find(session[:user_id])
		@gravatar = @user.gravatar
	end

	#this methods loads the edit user profile page
	def edit
		#get the user information from the database
		@user = User.find(session[:user_id])
	end

	#update users information in the database
	def update
		case params[:case]

		when "password"
			#confirm old password

			#save new password
		when "gravatar"
			#save new profile image to the database
			gravatar = Gravatar.find_by(user_id: session[:user_id])
			
			#get the uploaded file handle
			upload = params[:user][:gravatar]
	
			#compose the filename
			pic_name = session[:user_id].to_s + '_' + upload.original_filename
			pic_directory = 'assets/images/'
			
			if !gravatar.nil?

				File.open(Rails.root.join('app', pic_directory,
					pic_name), 'wb') do |file|

					file.write(upload.read)

				end

				gravatar.image = pic_name
				gravatar.save
				session[:gravatar] = pic_name

			else

				File.open(Rails.root.join('app', pic_directory,
					pic_name), 'wb') do |file|

					file.write(upload.read)

				end

				gravatar = Gravatar.new
				gravatar.user_id = session[:user_id]
				gravatar.image = pic_name
				gravatar.save
				session[:gravatar] = pic_name

			end

			flash[:success] = "Update Success!"
			redirect_to :back

		when "profile"					

			user = User.find_by(id: session[:user_id])
			input = params[:user]

			user.first_name = input[:first_name]
			user.last_name = input[:last_name]
			user.dob = input[:dob]
			user.save

			flash[:success] = "Update Success!"
			redirect_to :back

		when "details"

			user = User.find_by(id: session[:user_id])
			input = params[:user]

			user.h_phone = input[:h_phone]
			user.w_phone = input[:w_phone]
			user.p_address = input[:p_address]
			user.p_code = input[:p_code]
			user.city = input[:city]
			user.save

			flash[:success] = "Update Success!"
			redirect_to :back

		end
	end

	#log out the user and clear the session
	def logout

		#set the user's time out
		log = Log.find_by(user_id: session[:user_id])
		log.time_out = Time.now
		log.save

		#clear session and redirect user
		session[:user_id] = nil
		session[:gravatar] = nil
		session[:role] = nil

		flash[:success] = "Log Out Success!"
		redirect_to root_url
	end

	#load the login page
	def login
		if params[:user].nil?
			@user = User.new
			render 'login'
		else
			#athenticate user credentials
			@user = User.find_by(email: params[:user][:email])

			if !@user.nil?

				digest = Digest::MD5.new
				digest << params[:user][:password]

				if @user.password == digest.hexdigest #email and password pair correct
					if @user.activated == true
						if @user.throttle == nil || @user.throttle.suspended == false

							#reset throttle logs if they exist
							if @user.throttle != nil
								@user.throttle.attempts = 0
								@user.throttle.last = ""
								@user.throttle.suspended = false
								@user.throttle.save
							end

							#put user in logs
							@log = Log.new
							@log.user_id = @user.id
							@log.time_in = Time.now
							@log.ip_address = request.remote_ip()
							@log.save

							#get users gravatar
							if !@user.gravatar.nil?
								@gravatar = @user.gravatar.image
							else
								@gravatar = ""
							end

							#set user in session
							session[:user_id] = @user.id
							session[:gravatar] = @gravatar
							session[:role] = @user.role

							#check user role and redirect as appropriate
							if @user.role == "admin"
								redirect_to admin_url(session[:user_id])
							else
								redirect_to users_url
							end

						else
							#check if suspension is still valid or clear throttle
							time_sus = @user.throttle.time
							time_now = Time.now.to_s

							time_sus = time_sus.split(" ")
							time_sus = time_sus[0].split("-") + time_sus[1].split(":")
							time_sus = Time.gm(time_sus[0], time_sus[1], time_sus[2], time_sus[3], time_sus[4], time_sus[5])

							time_now = time_now.split(" ")
							time_now = time_now[0].split("-") + time_now[1].split(":")
							time_now = Time.gm(time_now[0], time_now[1], time_now[2], time_now[3], time_now[4], time_now[5])
							
							if (time_now.to_i - time_sus.to_i) >= 600 #if true, suspension expired
								
								#reset throttle logs
								@user.throttle.attempts = 0
								@user.throttle.last = ""
								@user.throttle.suspended = false
								@user.throttle.save

								#put user in logs
								@log = Log.new
								@log.user_id = @user.id
								@log.time_in = Time.now
								@log.ip_address = request.remote_ip()
								@log.save

								#get users gravatar
								if !@user.gravatar.nil?
									@gravatar = @user.gravatar.image
								else
									@gravatar = ""
								end

								#set user in session
								session[:user_id] = @user.id
								session[:gravatar] = @gravatar
								session[:role] = @user.role

								#check user role and redirect as appropriate
								if @user.role == "admin"
									redirect_to admin_url
								else
									redirect_to users_url
								end
							else
								flash[:danger] = "Account suspended. Please wait 10 minutes and try again!"							
							end
						end
					else
						flash[:danger] = 'Please confirm your email address first!'
					end
				else
					if @user.throttle != nil
						@throttle = @user.throttle
						@throttle.attempts += 1

						if @throttle.attempts >= 3
							@throttle.suspended = true
							flash[:danger] = "You Exceeded the maximum login attempts. Your account has been suspended! Wait up to 10 minutes and try again."
						else
							flash[:danger] = "Invalid Password supplied!"
						end

						#set last attempt time and account suspend time
						@throttle.last = Time.now
						@throttle.time = Time.now

						@throttle.save

					else
						#set attempt into throttle
						@throttle = Throttle.new
						@throttle.user_id = @user.id
						@throttle.attempts = 1
						@throttle.last = Time.now
						@throttle.suspended = false
						@throttle.save

						flash[:danger] = "Invalid Password supplied!"
					end
				end
			else
				#get instance of new user for the login form helper
				@user = User.new
				@user.email = params[:user][:email]
				@user.password = params[:user][:password]

				flash[:danger] = "User not found!"
			end
		end
	end

	#reset the user's password
	def password
		if params[:token].nil?
			render 'forgot_password'
		else
			#validate user id and token pair
		end
	end

	def checkemail
		check = Hash.new
		user = User.find_by(email: params[:email])

		if !user.nil?
			check[:found] = true
			check[:email] = params[:email]
		else
			check[:found] = false
			check[:email] = params[:email]
		end

		render json: check

	end

	private
		#filter acceptable parameters from the user input 
		def user_params
			params.require(:user[:email]).permit(:first_name, :last_name, :password, :password_confirm)
		end
end
