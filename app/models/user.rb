class User < ActiveRecord::Base
	has_one :gravatar, dependent: :destroy
	has_one :throttle, dependent: :destroy
	has_many :logs, dependent: :destroy
end
