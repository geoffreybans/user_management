class UserMailer < ApplicationMailer
	default from: "notifications@example.com"

	def welcome_email(user)
		@user = user
		@url = "/users/confirm/#{@user.id}/#{@user.activation_code}"

		mail(to: @user.email, subject: "Thanks for signing up! Please confirm your email...")
	end
end
