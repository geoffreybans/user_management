class AdminController < ThrottleController

	#show admin dashboard
	def index
		@online = Log.where(time_out: nil).count
		@users = User.includes(:gravatar).all
		@user = User.find(session[:user_id])
		@gravatar = @user.gravatar
	end

	#show application settings
	def show
		@online = Log.where(time_out: nil).count
		@users = User.includes(:gravatar).all
		@user = User.find(session[:user_id])
		@gravatar = @user.gravatar
	end

	#load edit settings page
	def edit
		@user = User.find(session[:user_id])
		@gravatar = @user.gravatar
		@settings = Setting.last
	end

	#update application settings
	def update
		#save new settings into the database
	end

	#deactivate a  user's account
	def activate
		if params[:action] == 'activate'
			#activate
		else
			#deactivate
		end
	end

	#generate user's report in .pdf
	def report
		if !params[:user_id].nil?
			#get reports per user
		else
			#get reports for all users
		end
	end

	def setuser
		case params[:mode]
		
		when "active"
			user = User.find_by(id: params[:id])

			user.activated = true
			user.activation_code = nil
			user.save

			flash[:success] = "User Activation Success!"
			redirect_to admin_index_path

		when "inactive"
			user = User.find_by(id: params[:id])

			user.activated = false
			user.activation_code = nil
			user.save

			flash[:success] = "User De-activation Success!"
			redirect_to admin_index_path
		
		when "admin"
			user = User.find_by(id: params[:id])

			user.role = "admin"
			user.save

			flash[:success] = "Success! User set to admin."
			redirect_to admin_index_path

		when "user"
			user = User.find_by(id: params[:id])

			user.role = "user"
			user.save

			flash[:success] = "Success! User set to regular user"
			redirect_to admin_index_path
		when "delete"
			user = User.find_by(id: params[:id])
			user.destroy

			redirect_to admin_index_path
		end
	end

	def logs
		@page = params[:page] || 1
		@page = @page.to_i
		@limit = params[:rows] || 10
		@limit = @limit.to_i

		@count = Log.count
		@pages = @count / @limit + 1

		offset = (@page - 1) * @limit + 1
		@logs = Log.includes(:user).limit(@limit).offset(offset)
		@user = User.find(session[:user_id])
	end

	def viewuser
		@user = User.includes(:gravatar).find_by(id: params[:id])
		@logs = Log.where(user_id: params[:id]).last(3)
	end
end